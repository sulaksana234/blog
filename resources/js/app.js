require('./bootstrap');



import Dashboard from './views/Board'


const router = new VueRouter({
    mode: 'history',
    routes: [
      
     
        {
            path: '/board',
            name: 'board',
            component: Dashboard,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});