@extends('adminlte::page')

@section('title', 'Data Roles')

@section('content_header')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <h2>Permissions Management</h2>
            </div>
            <div class="col-md-7" style="float:right;text-align:right">
                <a class="btn btn-primary btn-sm" href="{{route('permissions.create')}}" ><i class="fas fa-plus"></i> Create New Permissions</a>

            </div>
        </div>
    </div>
</div>

@stop

@section('content')

<div class="card">
    <div class="card-body">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>Deskripsi</th>
                    

                        <th width="280px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>



@stop

@section('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


<style>
    label.error {
        color: #dc3545;
        font-size: 14px;
    }
</style>
@stop

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
   <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>


<script type="text/javascript">
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      

        var table = $('.data-table').DataTable({
            processing: "<i class='fas fa-spinner fa-spin fa-3x fa-fw'></i>Processing...",
            serverSide: true,
            ajax: "{{ route('permissions.index') }}",
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'display_name',
                    'render': function ( data) {
                        if(data != null){
                            return '<h5>'+data+'</h5>';
                        }else{
                            return '<h5 style="color:red">Display Name belum diinput</h5>';
                        }
                    },
                },
                {
                    data: 'description',
                    'render': function ( data) {
                        if(data != null){
                            return '<h5>'+data+'</h5>';
                        }else{
                            return '<h5 style="color:red">Deskripsi belum diinput</h5>';
                        }
                    },
                },
              
              
               
               
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
    
       
           $('body').on('click', '#deleteRole', function () {

        let role_id = $(this).data('id');
        let token   = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'Apakah Kamu Yakin?',
            text: "ingin menghapus data ini!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'TIDAK',
            confirmButtonText: 'YA, HAPUS!'
        }).then((result) => {
            if (result.value) {


                //fetch to delete data
                $.ajax({

                    url: `/roles/${role_id}`,
                    type: "DELETE",
                    cache: false,
                    data: {
                        "_token": token
                    },
                    success:function(response){ 
                        
                        //show success message
                        Swal.fire({
                            type: 'success',
                            type: 'success',
                            title: `${response.message}`,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        table.ajax.reload();


                        //remove post on table
                        $(`#index_${role_id}`).remove();
                    }
                });

                
            }
        })
        
    });





    });
</script>
@stop