@extends('adminlte::page')

@section('title', 'Form User')

@section('content_header')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <h2>Form Permissions</h2>
            </div>
            <div class="col-md-7" style="float:right;text-align:right">
                <a class="btn btn-primary" href="{{ route('permissions.index') }}"><i class="fas fa-arrow-left"></i> Back</a>

            </div>
        </div>
    </div>
</div>

@stop

@section('content')
@if(session('status'))
<div class="alert alert-success mb-1 mt-1">
    {{ session('status') }}
</div>
@endif
<div class="card">
    <div class="card-body">
        <form action="{{ route('permissions.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label class="font-weight-bold">Permissions Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name">

                        <!-- error message untuk title -->
                        @error('name')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label class="font-weight-bold">Permissions Display</label>
                        <input type="text" class="form-control @error('display_name') is-invalid @enderror" name="display_name" placeholder="Display Name">

                        <!-- error message untuk title -->
                        @error('display_name')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Description:</strong>
                        <input type="text" name="description" class="form-control" placeholder="Deskripsi Role">
                        @error('nik')
                        <div class="alert alert-danger mt-1 mb-1">{{ $description }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
            </div>
        </form>
    </div>
</div>


@stop

@section('css')
<style>
    label.error {
        color: #dc3545;
        font-size: 14px;
    }

    .login_oueter {
        width: 360px;
        max-width: 100%;
    }

    .logo_outer {
        text-align: center;
    }

    .logo_outer img {
        width: 120px;
        margin-bottom: 40px;
    }
</style>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    function password_show_hide() {
        var x = document.getElementById("password");
        var show_eye = document.getElementById("show_eye");
        var hide_eye = document.getElementById("hide_eye");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>
@stop