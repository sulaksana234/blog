@extends('adminlte::page')

@section('title', 'Form User')

@section('content_header')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <h2>Form User</h2>
            </div>
            <div class="col-md-7" style="float:right;text-align:right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"><i class="fas fa-arrow-left"></i> Back</a>

            </div>
        </div>
    </div>
</div>

@stop

@section('content')
@if(session('status'))
<div class="alert alert-success mb-1 mt-1">
    {{ session('status') }}
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card border-0 shadow rounded">
            <div class="card-body">
                <form action="{{ route('users.update', $users->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="id" id="id" value="{{$users->id}}">
                    <div class="form-group">
                        <label class="font-weight-bold">Profile</label>
                        <input type="file" class="form-control" name="profile" id="profile">
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">NIK</label>
                        <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik', $users->warga_banjar->nik != null) }}" placeholder="Masukkan Nik Anda">

                        <!-- error message untuk title -->
                        @error('nik')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $users->name) }}" placeholder="Masukkan Nama">

                        <!-- error message untuk title -->
                        @error('name')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">Tempat Tanggal Lahir</label>
                        <input type="text" class="form-control @error('tempat_tanggal_lahir') is-invalid @enderror" name="tempat_tanggal_lahir" value="{{ old('tempat_tanggal_lahir', $users->warga_banjar->tempat_tanggal_lahir != null ? $users->warga_banjar->tempat_tanggal_lahir : '') }}" placeholder="Masukkan Tempat Tanggal Lahir">

                        <!-- error message untuk title -->
                        @error('tempat_tanggal_lahir')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">Email</label>
                        <input class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Masukkan Email" value="{{ old('email', $users->email) }}"></textarea>

                        <!-- error message untuk content -->
                        @error('email')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Tempekan</label>
                        <select name="tempekan" id="tempekan" class="form-control">
                            <option {{old('tempekan',$users->warga_banjar->tempekan)=="bima"? 'selected':''}} value="bima">BIMA</option>
                            <option {{old('tempekan',$users->warga_banjar->tempekan)=="arjuna"? 'selected':''}} value="arjuna">ARJUNA</option>
                            <option {{old('tempekan',$users->warga_banjar->tempekan)=="nakula"? 'selected':''}} value="nakula">NAKULA</option>
                            <option {{old('tempekan',$users->warga_banjar->tempekan)=="sahadewa"? 'selected':''}} value="sahadewa">SAHADEWA</option>
                            <option {{old('tempekan',$users->warga_banjar->tempekan)=="yudistira"? 'selected':''}} value="yudistira">YUDISTIRA</option>
                        </select>


                        <!-- error message untuk content -->
                        @error('tempekan')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jenis kelamin" class="font-weight-bold">Jenis Kelamin</label>
                        <div class="radio ">
                            <label><input name="jenis_kelamin"  {{ $users->warga_banjar->jenis_kelamin == 'laki-laki' ? 'checked' : '' }} type="radio" value="laki-laki">Laki - Laki</label>
                        </div>
                        <div class="radio ">
                            <label><input name="jenis_kelamin" type="radio" {{ $users->warga_banjar->jenis_kelamin == 'perempuan' ? 'checked' : '' }} value="perempuan">Perempuan</label>
                        </div>
                        @error('jenis_kelamin')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Agama</label>
                        <select name="agama" id="agama" class="form-control">
                            <option {{old('agama',$users->warga_banjar->agama)=="hindu"? 'selected':''}} value="hindu">HINDU</option>
                            <option {{old('agama',$users->warga_banjar->agama)=="islam"? 'selected':''}} value="islam">ISLAM</option>
                            <option {{old('agama',$users->warga_banjar->agama)=="kristen"? 'selected':''}} value="kristen">KRISTEN</option>
                            <option {{old('agama',$users->warga_banjar->agama)=="protestan"? 'selected':''}} value="protestan">PROTESTAN</option>
                            <option {{old('agama',$users->warga_banjar->agama)=="budha"? 'selected':''}} value="budha">BUDHA</option>
                            <option {{old('agama',$users->warga_banjar->agama)=="konghucu"? 'selected':''}} value="konghucu">KONGHUCU</option>

                        </select>


                        <!-- error message untuk content -->
                        @error('agama')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Pekerjaan</label>
                        <select name="pekerjaan" id="pekerjaan" class="form-control">
                            <option {{old('pekerjaan',$users->warga_banjar->pekerjaan)=="pegawai_swasta"? 'selected':''}} value="pegawai_swasta">Pegawai Swasta</option>
                            <option {{old('pekerjaan',$users->warga_banjar->pekerjaan)=="pns"? 'selected':''}} value="pns">PNS</option>
                        </select>
                        <!-- error message untuk content -->
                        @error('agama')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Alamat Banjar</label>
                        <textarea class="form-control @error('alamat_banjar') is-invalid @enderror" name="alamat_banjar" rows="5" placeholder="Masukkan alamat banjar">{{ old('alamat_banjar', $users->warga_banjar->alamat_banjar) }}</textarea>

                        <!-- error message untuk content -->
                        @error('alamat_banjar')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Alamat Lengkap</label>
                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" rows="5" placeholder="Masukkan alamat">{{ old('alamat', $users->alamat) }}</textarea>

                        <!-- error message untuk content -->
                        @error('alamat')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-md btn-primary"><i class="fas fa-save"></i> UPDATE</button>

                </form>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
<style>
    label.error {
        color: #dc3545;
        font-size: 14px;
    }
</style>
@stop

@section('js')

@stop