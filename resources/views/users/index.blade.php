@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <h2>Users Management</h2>
            </div>
            <div class="col-md-7" style="float:right;text-align:right">
                <a class="btn btn-primary btn-sm" href="{{route('users.create')}}" ><i class="fas fa-plus"></i> Create New User</a>

            </div>
        </div>
    </div>
</div>

@stop

@section('content')

<div class="card">
    <div class="card-body">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Username</th>

                        <th>Email</th>
                        <th>Profile</th>

                        <th width="280px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>



@stop

@section('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


<style>
    label.error {
        color: #dc3545;
        font-size: 14px;
    }
</style>
@stop

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
   <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>


<script type="text/javascript">
    $(document).ready(function() {})
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      

        var table = $('.data-table').DataTable({
            processing: "<i class='fas fa-spinner fa-spin fa-3x fa-fw'></i>Processing...",
            serverSide: true,
            ajax: "{{ route('users.index') }}",
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'username',
                    name: 'username'
                },
             
                {
                    data: 'email',
                    name: 'email'
                },
              
               
             
             
               
                
               
                { data: 'profile' ,
              'render': function ( data) {
                if(data != null){
                    return '<img src="http://blog.local.com:82/images/'+data+'" width="40px">';
                }else{
                            return '<h5 style="color:red">Data Profile Belum Diinput</h5>';
                        }

                }
            },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
    
       






    });
</script>
@stop