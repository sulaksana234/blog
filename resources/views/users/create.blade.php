@extends('adminlte::page')

@section('title', 'Form User')

@section('content_header')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <h2>Form User</h2>
            </div>
            <div class="col-md-7" style="float:right;text-align:right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"><i class="fas fa-arrow-left"></i> Back</a>

            </div>
        </div>
    </div>
</div>

@stop

@section('content')
@if(session('status'))
<div class="alert alert-success mb-1 mt-1">
    {{ session('status') }}
</div>
@endif
<div class="card">
    <div class="card-body">
        <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label class="font-weight-bold">Profile</label>
                        <input type="file" class="form-control @error('profile') is-invalid @enderror" name="profile">

                        <!-- error message untuk title -->
                        @error('profile')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>No KK:</strong>
                        <input type="text" name="nik" class="form-control" placeholder="NIK">
                        @error('nik')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>NIK:</strong>
                        <input type="text" name="nik" class="form-control" placeholder="NIK">
                        @error('nik')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
                        @error('name')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tempat Tanggal Lahir:</strong>
                        <input type="text" name="tempat_tanggal_lahir" class="form-control" placeholder="Tempat Tanggal Lahir">
                        @error('tempat_tanggal_lahir')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        @error('email')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">
                        <label class="font-weight-bold">Tempekan</label>
                        <select name="tempekan" id="tempekan" class="form-control">
                            <option value="bima">BIMA</option>
                            <option value="arjuna">ARJUNA</option>
                            <option value="nakula">NAKULA</option>
                            <option value="sahadewa">SAHADEWA</option>
                            <option value="yudistira">YUDISTIRA</option>
                        </select>


                        <!-- error message untuk content -->
                        @error('tempekan')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">
                        <label for="jenis kelamin" class="font-weight-bold">Jenis Kelamin</label>
                        <div class="radio ">
                            <label><input name="jenis_kelamin" type="radio" value="1">Laki - Laki</label>
                        </div>
                        <div class="radio ">
                            <label><input name="jenis_kelamin" type="radio" value="2">Perempuan</label>
                        </div>
                        @error('jenis_kelamin')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">
                        <label class="font-weight-bold">Agama</label>
                        <select name="agama" id="agama" class="form-control">
                            <option value="hindu">HINDU</option>
                            <option value="islam">ISLAM</option>
                            <option value="kristen">KRISTEN</option>
                            <option value="protestan">PROTESTAN</option>
                            <option value="budha">BUDHA</option>
                            <option value="konghucu">KONGHUCU</option>

                        </select>


                        <!-- error message untuk content -->
                        @error('agama')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label class="font-weight-bold">Pekerjaan</label>
                        <select name="pekerjaan" id="pekerjaan" class="form-control">
                            <option value="pegawai_swasta">Pegawai Swasta</option>
                            <option value="pns">PNS</option>
                        </select>
                        <!-- error message untuk content -->
                        @error('agama')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">
                        <label class="font-weight-bold">Alamat Banjar</label>
                        <textarea class="form-control @error('alamat_banjar') is-invalid @enderror" name="alamat_banjar" rows="5" placeholder="Masukkan alamat banjar"></textarea>

                        <!-- error message untuk content -->
                        @error('alamat_banjar')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Address:</strong>
                        <textarea name="alamat" class="form-control alamat" cols="50"></textarea>

                        @error('alamat')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                @can('role-select')
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Select Role:</strong>
                        @foreach ($dataRole as $dataRole)
                        <div>
                            <input type="checkbox" class="form-control form-control-sm" id="{{$dataRole->name}}" name="{{$dataRole->name}}">
                            <label class="font-weight-bold" for="horns">{{$dataRole->display_name}}</label>
                        </div>
                        @endforeach

                        @error('dataRole')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                @endcan
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Password:</strong>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                            </div>
                            <input name="password" type="password" value="" class="input form-control" id="password" placeholder="password"  aria-label="password" aria-describedby="basic-addon1" />
                            <div class="input-group-append">
                                <span class="input-group-text" onclick="password_show_hide();">
                                    <i class="fas fa-eye" id="show_eye"></i>
                                    <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                </span>
                            </div>
                        </div>
                        @error('password')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Confirm Password:</strong>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                            </div>
                            <input name="password_confirm" type="password" value="" class="input form-control" id="password_confirm" placeholder="Confirm Password"  aria-label="password" aria-describedby="basic-addon1" />
                            <div class="input-group-append">
                                <span class="input-group-text" onclick="password_show_hide();">
                                    <i class="fas fa-eye" id="show_eye"></i>
                                    <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                </span>
                            </div>
                        </div>
                        @error('password_confirm')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
            </div>
        </form>
    </div>
</div>


@stop

@section('css')
<style>
    label.error {
        color: #dc3545;
        font-size: 14px;
    }

    .login_oueter {
        width: 360px;
        max-width: 100%;
    }

    .logo_outer {
        text-align: center;
    }

    .logo_outer img {
        width: 120px;
        margin-bottom: 40px;
    }
</style>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    function password_show_hide() {
        var x = document.getElementById("password");
        var show_eye = document.getElementById("show_eye");
        var hide_eye = document.getElementById("hide_eye");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>
@stop