<?php

namespace App\Http\Controllers;

use App\Models\KelihanBanjar;
use App\Models\Pilihan;
use App\Models\User;
use App\Models\WargaBanjar;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $jumlahAnggotaTempekanKanginan = WargaBanjar::where('tempekan','=','kangin')->count();
        $jumlahAnggotaTempekanKanginanTengah = WargaBanjar::where('tempekan','=','tengah_kangin')->count();
        $jumlahAnggotaTempekanKauh = WargaBanjar::where('tempekan','=','kauh')->count();
        $jumlahAnggotaTempekanKauhTengah = WargaBanjar::where('tempekan','=','tengah_kauh')->count();
        $jumlahAnggotaLakiLaki = WargaBanjar::where('jenis_kelamin','=','laki-laki')->count();
        $jumlahAnggotaPerempuan = WargaBanjar::where('jenis_kelamin','=','perempuan')->count();
        $jumlahYangSudahMilih = number_format((Pilihan::count()/($jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan))*100,2, '.', '');
        $jumlahYangBelumMilih = number_format((($jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan-Pilihan::count())/$jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan)*100,2, '.', '');

        $kelihanBr = KelihanBanjar::all();

        return view('home',compact(
            'jumlahAnggotaTempekanKanginan',
            'jumlahAnggotaTempekanKanginanTengah',
            'jumlahAnggotaTempekanKauh',
            'jumlahAnggotaTempekanKauhTengah',
            'jumlahAnggotaLakiLaki',
            'jumlahAnggotaPerempuan',
            'jumlahYangSudahMilih',
            'jumlahYangBelumMilih',
            'kelihanBr'
        ));
    }
}
