<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WargaBanjar;
use Illuminate\Http\Request;
use DataTables;

class DataBanjarController extends Controller
{
    function __construct()
    {
        // set permission
        // $this->middleware('permission:user-list|user-create|user-edit|users-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:users-update', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $wargaBanjar = WargaBanjar::latest()->get();
            return Datatables::of($wargaBanjar)

                ->addColumn('action', function ($row) {

                    $btn = '<a href="/data_banjar/' . $row->id . '" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->user_id . '" data-original-title="Delete" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('DataPeserta.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('DataPeserta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...

            $request->validate([
                'id_ektp' => ['required', 'unique:warga_banjar'],
                'id_sidik_jari' => ['required', 'unique:warga_banjar'],
                'nik' => ['required', 'unique:warga_banjar', 'max:16', 'min:16'],
                'nama' => 'required',
                'alamat' => 'required',
                'tempat_tanggal_lahir' => 'required',
                'jenis_kelamin' => 'required',
                'tempekan' => 'required',

                'agama' => 'required',
                'pekerjaan' => 'required',
            ]);




            $dataWargaBanjar = [
                'id_ektp' => $request->id_ektp,
                'id_sidik_jari' => $request->id_sidik_jari,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'nik'   => $request->nik,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'tempekan' => $request->tempekan,
                'alamat_banjar' => $request->alamat_banjar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'agama' => $request->agama,
                'pekerjaan' => $request->pekerjaan,
            ];

            WargaBanjar::create($dataWargaBanjar);



            return redirect()->back()->with(['success' => 'User Berhasil ditambahkan !']);
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $dataWargaBanjar = WargaBanjar::find($id);
        return view('DataPeserta.edit', compact('dataWargaBanjar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        try {
            //code...
            $dataWargaBanjar = WargaBanjar::where('id', $request->id)->first();

            //validate form
            $this->validate($request, [
                'id_ektp' => ['required', 'unique:warga_banjar,id_ektp,' . $request->id],
                'id_sidik_jari' => ['required', 'unique:warga_banjar,id_sidik_jari,' . $request->id],
                'nik' => ['required', 'max:16', 'min:16'],
                'nama' => 'required',
                'alamat' => 'required',
                'alamat_banjar' => 'required',

                'tempat_tanggal_lahir' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'pekerjaan' => 'required',

            ]);
            $dataWargaBanjarUpdate = [
                'id_ektp' => $request->id_ektp,
                'id_sidik_jari' => $request->id_sidik_jari,
                'nama' => $request->nama,
                'alamat' => $request->alamat,

                'nik'   => $request->nik,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'tempekan' => $request->tempekan,
                'alamat_banjar' => $request->alamat_banjar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'agama' => $request->agama,
                'pekerjaan' => $request->pekerjaan,
            ];
            $dataWargaBanjar->update($dataWargaBanjarUpdate);
            return redirect()->back()->with(['success' => 'User Berhasil diubah !']);

        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
