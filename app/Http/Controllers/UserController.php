<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\WargaBanjar;
use Illuminate\Http\Request;
use DataTables;

class UserController extends Controller
{
    function __construct()
    {
        // set permission
        // $this->middleware('permission:user-read', ['only' => ['index', 'show']]);
        // $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:users-update', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $user = User::with('warga_banjar');
            return Datatables::of($user)

                ->addColumn('action', function ($row) {

                    $btn = '<a href="/users/' . $row->id . '" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser"><i class="fas fa-edit"></i></a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteUser"><i class="fas fa-trash"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $dataRole = Role::all();
        return view('users.create',compact('dataRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'profile' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'no_kk' => ['required', 'unique:users','max:16', 'min:16'],
            'nik' => ['required', 'unique:users','max:16', 'min:16'],
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users'],
            'alamat' => 'required',
            'tempat_tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'pekerjaan' => 'required',

            'password' => [
                'required',
                'string',
                'min:10',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'password_confirm' => 'required|same:password'
        ]);


        $imageName = time() . '.' . $request->profile->extension();

        $request->profile->move(public_path('images'), $imageName);

        $data = [
            'profile'     => $imageName,
            'name' => $request->name,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'password' => bcrypt($request->password),
          
            
        ];

        $dataUser = User::create($data);

        $dataWargaBanjar = [
            'user_id' => $dataUser->id,
            'nik'   => $request->nik,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'tempekan' => $request->tempekan,
                'alamat_banjar' => $request->alamat_banjar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'agama' => $request->agama,
                'pekerjaan' => $request->pekerjaan,
        ];

        $data = WargaBanjar::create($dataWargaBanjar);
        
        if(isset($request->superadministrator) == 'on'){
            $dataUser->attachRole(1);
        } if(isset($request->administrator) == 'on'){
            $dataUser->attachRole(2);
        } if(isset($request->user) == 'on'){
            $dataUser->attachRole(3);
        }

        return redirect()->back()->with(['success' => 'User Berhasil ditambahkan !']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $users = User::with('warga_banjar')->find($id);
        return view('users.edit', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $dataUser = User::find($request->id);
        $dataWargaBanjar = WargaBanjar::where('user_id',$request->id)->first();

        //validate form
        $this->validate($request, [
            'profile'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nik' => ['required', 'max:16', 'min:16'],
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users,email,' . $request->id],
            'alamat' => 'required',
            'tempat_tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'pekerjaan' => 'required',

        ]);


        //check if image is uploaded
        if ($request->hasFile('profile')) {

            //upload new image
            $imageName = time() . '.' . $request->profile->extension();
            $request->profile->move(public_path('images'), $imageName);

            //delete old image
            $path = public_path() . "/images/" . $dataUser->profile;
            unlink($path);

            //update post with new image
            $data = [
                'profile'     => $imageName,
                'name'     => $request->name,
                'email'   => $request->email,
                'alamat'   => $request->alamat,
            ];
            $dataUser->update($data);
            $dataWargaBanjarUpdate = [
                'nik'   => $request->nik,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'tempekan' => $request->tempekan,
                'alamat_banjar' => $request->alamat_banjar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'agama' => $request->agama,
                'pekerjaan' => $request->pekerjaan,
            ];
            $dataWargaBanjar->update($dataWargaBanjarUpdate);
        } else {

            $data =
                [
                    'name'     => $request->name,
                    'email'   => $request->email,
                    'alamat'   => $request->alamat,
                 
                ];

            //update post without image
            $dataUser->update($data);

            $dataWargaBanjarUpdate = [
                'nik'   => $request->nik,
                'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
                'tempekan' => $request->tempekan,
                'alamat_banjar' => $request->alamat_banjar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'agama' => $request->agama,
                'pekerjaan' => $request->pekerjaan,
            ];
            $dataWargaBanjar->update($dataWargaBanjarUpdate);
        }

        //redirect to index
        return redirect()->back()->with(['success' => 'User Berhasil Diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
