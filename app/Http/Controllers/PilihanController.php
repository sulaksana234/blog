<?php

namespace App\Http\Controllers;

use App\Models\Pilihan;
use App\Models\WargaBanjar;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\MyEvent;
class PilihanController extends Controller
{
    //
    public function jumlahSuara(Request $request){
        $jumlahAnggotaLakiLaki = WargaBanjar::where('jenis_kelamin','=','laki-laki')->count();
        $jumlahAnggotaPerempuan = WargaBanjar::where('jenis_kelamin','=','perempuan')->count();
        $jumlahYangSudahMilih = number_format((Pilihan::count()/($jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan))*100,2, '.', '');
        $jumlahYangBelumMilih = number_format((($jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan-Pilihan::count())/$jumlahAnggotaLakiLaki+$jumlahAnggotaPerempuan)*100,2, '.', '');

        return [
            "jumlahYangSudahMilih" => $jumlahYangSudahMilih,
            "jumlahYangBelumMilih" => $jumlahYangBelumMilih
        ];

    }
    public function pilihKelihanBr(Request $request){
        try {
            $dataPilihan = Pilihan::where('sidik_jari_user',Auth::user()->username)->first();
            if($dataPilihan == null){
                Pilihan::create([
                    'kelihan_id' => $request->id,
                    'sidik_jari_user' => Auth::user()->username,
                ]);
                $response = [
                    'status' => 200,
                ];
                event(new MyEvent('pilihkelihanbanjar'));

            }else{
                $response = [
                    'status' => 400,
                ];
                event(new MyEvent('pilihkelihanbanjar'));


            }

            return response()->json($response);

        } catch (\Throwable $th) {
            dd($th->getMessage());
            //throw $th;
            return $th->getMessage();
        }
    }
}
