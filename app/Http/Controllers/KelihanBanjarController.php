<?php

namespace App\Http\Controllers;

use App\Models\KelihanBanjar;
use Illuminate\Http\Request;
use DataTables;

class KelihanBanjarController extends Controller
{
    //
    public function index(Request $request){
        if ($request->ajax()) {
            $kelihanBr = KelihanBanjar::latest()->get();
            return Datatables::of($kelihanBr)
    
                ->addColumn('action', function ($row) {
    
                    $btn = '<a href="/kelihan_banjar/' . $row->id . '/edit" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>';
    
                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->user_id . '" data-original-title="Delete" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>';
    
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('KelihanBanjar.index');
    }
  
    public function create()
    {
        //
        return view('KelihanBanjar.create');
    }

    public function store(Request $request){
        $request->validate([
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required',
            'deskripsi' => 'required',
            'visible' => 'required',
         
        ]);

        $imageName = time() . '.' . $request->foto->extension();

        $request->foto->move(public_path('images'), $imageName);
        $data = [
            'foto'     => $imageName,
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'visible' => $request->visible,          
        ];

        $dataUser = KelihanBanjar::create($data);
        return redirect(route('kelihan_banjar.index'))->with(['success' => 'Calon kelian banjar berhasil ditambahkan !']);


    }
    public function edit($id)
    {
        //
        $dataKelihan = KelihanBanjar::find($id);
        return view('KelihanBanjar.edit', compact('dataKelihan')); 
    }

    public function update(Request $request){
        $dataKelihanBanjar = KelihanBanjar::find($request->id);

        $this->validate($request, [
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nama' => 'required',
            'deskripsi' => 'required',
            'visible' => 'required',
        ]);
        if ($request->hasFile('foto')) {

            //upload new image
            $imageName = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images'), $imageName);

            //delete old image
            $path = public_path() . "/images/" . $dataKelihanBanjar->foto;
            unlink($path);

            //update post with new image
            $data = [
                'foto'     => $imageName,
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'visible' => $request->visible,        
            ];
            $dataKelihanBanjar->update($data);
         
        } else {

            $data = [
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'visible' => $request->visible,        
            ];

            //update post without image
            $dataKelihanBanjar->update($data);

           
        }

        //redirect to index
        return redirect(route('kelihan_banjar.index'))->with(['success' => 'Calon kelian banjar berhasil ditambahkan !']);
    }
}
