<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use DataTables;

class PermissionsController extends Controller
{
    //
    public function index(Request $request){
        if ($request->ajax()) {
            $role = Permission::latest()->get();
            return Datatables::of($role)

                ->addColumn('action', function ($row) {

                    $btn = '<a href="/roles/' . $row->id . '/edit" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser"><i class="fas fa-edit"></i></a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" id="deleteRole" data-original-title="Delete" class="btn btn-danger btn-sm deleteUser"><i class="fas fa-trash"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('permissions.index');
    }
    public function create()
    {
        //
        return view('permissions.create');
    }
    public function store(Request $request)
    {
        //
        $data = $request->validate([
            'name' => 'required|string|unique:roles,name',
            'display_name' => 'nullable|string',
            'description' => 'nullable|string',
        ]);

        $permissions = Permission::create($data);

        return redirect(route('permissions.index'))->with(['success' => 'Permissions Berhasil ditambahkan !']);
    }
}
