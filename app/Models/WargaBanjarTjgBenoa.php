<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WargaBanjarTjgBenoa extends Model
{
    protected $table = 'warga_banjar_tjg_benoa';
    public $guarded = [];
}
