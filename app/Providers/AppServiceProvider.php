<?php

namespace App\Providers;

use App\Helper\NewGate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);
    }
}
