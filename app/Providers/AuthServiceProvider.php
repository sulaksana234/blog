<?php

namespace App\Providers;

use App\Models\User;
// use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Gate;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        'App\Models\User' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        // $this->registerPolicies();

        //
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
      
            
            $event->menu->add([
                'text' => 'Chat',
                'url' => '/home',
                'icon' => 'fas fa-user',
            ]);
        
            $event->menu->add([
                'text' => 'Data User',
                'url' => '/users/',
                'icon' => 'fas fa-user',
                'can' => 'users-read'
            ]);
         
        

            $event->menu->add([
                'text'    => 'Master Data',
                'icon'    => 'fas fa-fw fa-server',
                'submenu' => [
                    [
                        'text' => 'Roles',
                        'icon' => 'fas fa-users',
                        'url'  => 'roles',
                        'can' =>'role-list'
                    ],
                    [
                        'text' => 'Permissions',
                        'icon' => 'fas fa-users',
                        'url'  => 'permissions',
                    ],
                    [
                        'text' => 'Agama',
                        'icon' => 'fas fa-users',
                        'url'  => 'agama',
                    ],
                ]
            ]);
        });



        // ['header' => 'account_settings'],
        // [
        //     'text' => 'Data Peserta',
        //     'url'  => 'data_banjar',
        //     'icon' => 'fas fa-fw fa-user',
        //     'permission'  => 'data-list',

        // ],
        // [
        //     'text' => 'user',
        //     'url'  => 'users',
        //     'icon' => 'fas fa-fw fa-user',
        //     'can' => 'users-read'
        // ],
        // [
        //     'text' => 'change_password',
        //     'url'  => 'admin/settings',
        //     'icon' => 'fas fa-fw fa-lock',
        // ],
        // [
        //     'text'    => 'Master Data',
        //     'icon'    => 'fas fa-fw fa-server',
        //     'submenu' => [
        //         [
        //             'text' => 'Roles',
        //             'icon' => 'fas fa-users',
        //             'url'  => '#',
        //         ],
        //         [
        //             'text'    => 'level_one',
        //             'url'     => '#',
        //             'submenu' => [
        //                 [
        //                     'text' => 'level_two',
        //                     'url'  => '#',
        //                 ],
        //                 [
        //                     'text'    => 'level_two',
        //                     'url'     => '#',
        //                     'submenu' => [
        //                         [
        //                             'text' => 'level_three',
        //                             'url'  => '#',
        //                         ],
        //                         [
        //                             'text' => 'level_three',
        //                             'url'  => '#',
        //                         ],
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         [
        //             'text' => 'level_one',
        //             'url'  => '#',
        //         ],
        //     ],
        // ],


    }
}
