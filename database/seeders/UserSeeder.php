<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User() ;
        $admin->name = "admin";  
        $admin->email = "admin@gmail.com";
        $admin->password=bcrypt('12345');
        $admin->save();

      
        $admin->attachRole('superadministrator');
    }
}
