<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\WargaBanjar;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WargaBanjarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id_ID');

        for ($i = 0 ;$i < 150; $i++){
            $dataWargaBanjar = WargaBanjar::create([
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'id_ektp' => '352501520188'.$i,
                'id_sidik_jari' => '352501520188'.$i,
                'nik' => '352501520188'.$i,
                'tempat_tanggal_lahir' => 'Badung, 23 Desember 1998',
                'tempekan' => 'kangin',
                'alamat_banjar' => 'Tanjung Benoa',
                'agama' => 'Hindu',
                'jenis_kelamin' => 'laki-laki',
                'pekerjaan' => 'pegawai_swasta',
            ]);
            User::create([
                'username' => $dataWargaBanjar->id_sidik_jari,
                'name' => $dataWargaBanjar->nama,
                'email' => $faker->email,
                'password' => bcrypt('12345'),
            ])->attachRole('user');
        }

     
        for ($i = 0 ;$i < 150; $i++){
            $dataWargaBanjar = WargaBanjar::create([
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'id_ektp' => '352501520188'.$i,
                'id_sidik_jari' => '352501520188'.$i,
                'nik' => '352501520188'.$i,
                'tempat_tanggal_lahir' => 'Badung, 23 Desember 1998',
                'tempekan' => 'tengah_kangin',
                'alamat_banjar' => 'Tanjung Benoa',
                'agama' => 'Hindu',
                'jenis_kelamin' => 'laki-laki',
                'pekerjaan' => 'pegawai_swasta',
            ]);
            User::create([
                'username' => $dataWargaBanjar->id_sidik_jari,
                'name' => $dataWargaBanjar->nama,
                'email' => $faker->email,
                'password' => bcrypt('12345'),
            ])->attachRole('user');
        }

     

        for ($i = 0 ;$i < 150; $i++){
            $dataWargaBanjar = WargaBanjar::create([
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'id_ektp' => '352501520188'.$i,
                'id_sidik_jari' => '352501520188'.$i,
                'nik' => '352501520188'.$i,
                'tempat_tanggal_lahir' => 'Badung, 23 Desember 1998',
                'tempekan' => 'kauh',
                'alamat_banjar' => 'Tanjung Benoa',
                'agama' => 'Hindu',
                'jenis_kelamin' => 'laki-laki',
                'pekerjaan' => 'pegawai_swasta',
            ]);
            User::create([
                'username' => $dataWargaBanjar->id_sidik_jari,
                'name' => $dataWargaBanjar->nama,
                'email' => $faker->email,
                'password' => bcrypt('12345'),
            ])->attachRole('user');
        }

       

        for ($i = 0 ;$i < 150; $i++){
            $dataWargaBanjar = WargaBanjar::create([
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'id_ektp' => '352501520188'.$i,
                'id_sidik_jari' => '352501520188'.$i,
                'nik' => '352501520188'.$i,
                'tempat_tanggal_lahir' => 'Badung, 23 Desember 1998',
                'tempekan' => 'tengah_kauh',
                'alamat_banjar' => 'Tanjung Benoa',
                'agama' => 'Hindu',
                'jenis_kelamin' => 'laki-laki',
                'pekerjaan' => 'pegawai_swasta',
            ]);
            User::create([
                'username' => $dataWargaBanjar->id_sidik_jari,
                'name' => $dataWargaBanjar->nama,
                'email' => $faker->email,
                'password' => bcrypt('12345'),
            ])->attachRole('user');
        }


      


     
      
        
    }
}
