<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWargaBanjarTjgBenoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warga_banjar_tjg_benoa', function (Blueprint $table) {
            $table->id();
         

          
            $table->integer('user_id');
            $table->string('nik',16)->nullable();
            $table->string('tempat_tanggal_lahir')->nullable();
            $table->string('tempekan')->nullable();
            $table->string('alamat_banjar')->nullable();
            $table->string('agama')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warga_banjar_tjg_benoa');
    }
}
