<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth','verifiedmiddleware']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('users', \App\Http\Controllers\UserController::class);
    Route::resource('data_banjar', \App\Http\Controllers\DataBanjarController::class);
    Route::resource('roles', \App\Http\Controllers\RoleController::class);
    Route::resource('kelihan_banjar', \App\Http\Controllers\KelihanBanjarController::class);
    Route::resource('permissions', \App\Http\Controllers\PermissionsController::class);
    Route::post('pilihKelihanBr', [App\Http\Controllers\PilihanController::class, 'pilihKelihanBr'])->name('pilihKelihanBr');
    Route::get('jumlahSuara', [App\Http\Controllers\PilihanController::class, 'jumlahSuara'])->name('jumlahSuara');

    Route::get('/conversations' , 'ConversationController@index')->name('conversations.index');
    Route::get('/conversations/{conversation}' , 'ConversationController@show')->name('conversation.show');

});
